import { errorStatus } from "../constants/statuses";
import { sessions, Session } from "../model/session";
import { loggerMessage } from "./logger";

export function currentSessionExist(session_key: string) {
  return sessions.map((session: Session) => session.session_key).indexOf(session_key);
}

export function updateSession(existSession: number) {
  sessions[existSession].counter++

  const existMessage = `Exist session: ${sessions[existSession].session_key}\n`
  const existMessageCount = `Exist session count: ${sessions[existSession].counter}\n`
  loggerMessage(`${existMessage}${existMessageCount}`)
}

export function removeSession(ctx) {
  ctx.cookies.set(
    'session_key',
    null,
    { httpOnly: true }
  );

  loggerMessage(`\n\nSorry user session should be removed\n\n`)
}

export function sessionMiddleware(ctx, next) {
  const session_key = ctx.cookies.get('session_key');
  const responseStatus = ctx.response.status;

  if (responseStatus != errorStatus) {
    if (!session_key) {
      const counter = 0;
      const currentDate = new Date();
      const newSessionKey = new Buffer(JSON.stringify({counter, currentDate})).toString('base64')

      sessions.push({
        session_key: newSessionKey,
        counter: 1
      })

      ctx.cookies.set(
        'session_key',
        newSessionKey,
        { httpOnly: true }
      );
      loggerMessage(`Session is created: ${currentDate}: ${newSessionKey}\n`)
    }
    else {
      const existSession = currentSessionExist(session_key)
      existSession >= 0 ?
        updateSession(existSession):
        removeSession(ctx);
    }
  }

  next();
}
