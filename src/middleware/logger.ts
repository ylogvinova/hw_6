const fs = require('fs')

export function logger(ctx, next) {
  const method = ctx.request.method
  const url = ctx.request.url
  const status = ctx.response.status
  const message = ctx.response.message

  const writeToFile = `User method: ${method}, url: ${url}, status: ${status}, message: ${message}\n\n`

  fs.appendFile('requestHistory.txt', writeToFile, () => {});
  next()
}

export function loggerMessage(message) {
  fs.appendFile('sessionHistory.txt', message, () => {});
}
