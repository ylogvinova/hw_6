export interface User {
  name: string;
  id: number;
  password: string;
}

export const userArray: Array<User> = [
  {id: 1, name: 'Dima', password: '123'}
]
