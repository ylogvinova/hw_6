export interface Session {
  session_key: string;
  counter: number;
}

export let sessions: Array<Session> = [];
