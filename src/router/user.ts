import * as Router  from 'koa-router';
import { loginHandle, sendUserList } from '../controller/user';

const router = new Router({ prefix: '/user' });

router.get('/', (ctx, next) => {
  ctx.body = sendUserList()
  next()
});

router.post('/login', loginHandle);

export default router;
