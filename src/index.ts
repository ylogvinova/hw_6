import * as Koa from 'koa';
import { sessionMiddleware } from './middleware/session';
import { logger } from './middleware/logger';
import userRouter from './router/user';
import * as bodyParser from 'koa-bodyparser';

const app = new Koa();

const APP_PORT = process.env.PORT;

app.use(bodyParser());

app.use(userRouter.routes());

app.use(sessionMiddleware);

app.use(logger);

app.listen(APP_PORT, () => {
  console.log('server is listening on port ', APP_PORT);
});
