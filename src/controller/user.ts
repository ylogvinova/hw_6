import { userArray, User } from '../model/user';

function isUserExist(user) {
  const existedUser = userArray.find(({id}) => id === user.id);
  return !!existedUser && existedUser.password === user.password
}

export function loginHandle(ctx, next) {
  const user = ctx.request.body as unknown as User;
  const isExist = isUserExist(user)

  if (isExist) {
    ctx.body = { user }
    ctx.status = 200;
  } else {
    ctx.body = { registered: false };
    ctx.status = 404;
  }

  next()
}

export function sendUserList() {
 return userArray
}
